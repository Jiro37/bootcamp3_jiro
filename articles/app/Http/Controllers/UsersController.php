<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UsersController extends Controller
{
    //
    public function createUsers(){
    	return view('users.create');
    }

    public function storeUsers(Request $request){
    	$this->validate($request, [

    		'name' => 'required',
    		'email' => 'required',
    		'password' => 'required',
    		'phone_no' => 'required',
    	]);

    	User::saveUsers($request);

        return redirect()->route('login');
    }

    public function loginUsers(){
    	return view('users.login');
    }

    public function findUsers(Request $request){
    	 if (Auth::attempt([
    	 	'email' => $request['email'], 
    	 	'password' => $request['password']
    	 ])) {
            // Authentication passed...
            return redirect()->route('posts.index');
        }else{
            return "Wrong Email or Password";
        }
    }

    public function logout (Request $request){
        Auth::logout();
        return redirect()->route('home');
    }
}
