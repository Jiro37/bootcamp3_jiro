@extends('main')

@section('title', '| Edit Post')

@section('content')
	<form action="{{ route('posts.update', $post->id) }}" method="POST">
		{{csrf_field()}} {{method_field('PATCH')}}
	<div class="row">
		
	<div class="col-md-8">
	<label>Title:</label>
	<input type="text" name="title" value="{{ $post->title }}" class="form-control">

	<label>Body:</label>
	<textarea rows="5" name="body" class="form-control">{{ $post->body }}</textarea>
	
	</div>
		<div class="col-md-4">
			<div class="card card-body bg-light p-3">
				<dl class="dl-horizontal">
					<dt>Created At:</dt>
					<dd>{{ date('M j, Y h:ia', strtotime($post->created_at))}}</dd>
				</dl>
				<dl class="dl-horizontal">
					<dt>Last Update:</dt>
					<dd>{{date('M j, Y h:ia', strtotime($post->created_at))}}</dd>
				</dl>
				<hr>

				<div class="row">
					<div class="col-sm-6">
						<a href="{{ route('posts.index') }}" class="btn btn-danger btn-block">Cancel</a>
					</div>
					<div class="col-sm-6">
						<input type="submit" class="btn btn-info btn-block" id="submit" name="submit" value="Update">
						
					</div>
				</div>

			</div>
		</div>
		
	</div>
	</form>
@endsection