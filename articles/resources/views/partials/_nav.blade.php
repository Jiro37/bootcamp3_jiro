<nav class="navbar navbar-expand-lg navbar-dark bg-dark ">  
  <div class="container">
  <a class="navbar-brand" href="#">
    <img src="/image/logo.svg" width="30" height="30" class="d-inline-block align-top" alt="">
    JirO
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href=" {{ route('home')}} ">Home <span class="sr-only">(current)</span></a>
      </li>

       @if( Auth::check())

           <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Account
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="{{route('posts.index')}}">All Posts</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="{{route('log-out')}}">Sign Out</a>
        </div>
      </li>

     
     @endif
   
      
    </ul>

 @if( !Auth::check())
<ul class="navbar-nav  my-2 my-lg-0">
    <li class="nav-item">
      <a class="nav-link" href=" {{route('login')}} ">Sign In</a>
    </li>
    <li class="nav-item" style="margin-top: 6px;">||</li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('register')}}">Sign Up</a>
    </li>
    </ul>
@endif
   
  </div>
</div> {{-- end of container --}}
</nav>