@extends('main')

@section('stylesheets')
   <link href="{{ asset('/css/style.css')}}" rel="stylesheet">
@endsection




@section('content')



<div class="row">

  <div class="col-md-5 col-sm-0">
    <picture>
      <img src="/image/logo.svg" class="login-logo img-fluid" />
    </picture>
    
  </div>

  <div class="col-md-7 col-sm-12" >
	<form action="{{ route('find-user')}}" method="post">
		{{csrf_field()}}

		@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

		<div class="card card-body">
      <h5 class="card-title ml7">
        <span class="text-wrapper">
        <span class="letters">Sign In</span>
        </span>
      </h5>

  <div class="form-group">
    <label for="email">Email address</label>
    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
    <small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
  </div>

  <button type="submit" class="btn btn-primary">SIGN IN</button>
</div>
</form>
 	
 	</div>

  
 </div>  
 <br>
 <br>
@section('scripts')

  <script src=" {{ asset('js/anime.min.js') }}"></script>

  <script src=" {{ asset('/js/style.js') }}"></script>
@endsection

@endsection