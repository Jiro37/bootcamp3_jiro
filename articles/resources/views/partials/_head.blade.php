<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bens Blog @yield('title')</title>

    <!-- Bootstrap core CSS -->
<!--     <link href="/css/bootstrap.min.css" rel="stylesheet">
 -->    

 <link href="{{ asset('css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom styles for this template -->
    
<!--     <link rel="stylesheet" type="text/css" href="/css/style.css">
 -->    

 <link href="{{ asset('css/style.css')}}" rel="stylesheet">


    

  </head>