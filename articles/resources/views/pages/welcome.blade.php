@extends('main')

@section('content')


{{--    <div class="row justify-content-center">
      <div class="col-md-10">
            <div class="jumbotron">
              <h1 class="my-4">Welcome to Bens Article Page <br>
              <h5>Thank you so much for visiting</h5><h4>Register To Post Your Articles <a href="{{ route('register') }}">Click Here</a></h4>
                
              </h1>
            </div>

        <div>
          <h2>View All articles</h2>
        </div>

         @foreach ($posts as $post)

             <!-- Blog Post -->
          <div class="card mb-4">
            
            <div class="card-body">
              <img src="{{$post->image}}">
              <h2 class="card-title">{{$post->title}}</h2>
              <p class="card-text">{{ substr($post->body, 0 ,300) }}{{ strlen($post->body) > 300 ? "...": "" }}</p>
              <a href="#" class="btn btn-primary">Read More &rarr;</a>
            </div>
            <div class="card-footer text-muted">
              Posted on {{ date('M j, Y ', strtotime($post->created_at))}} by
              <a href="#">{{ $post->user->name}}</a>
            </div>
          </div>
          @endforeach
          
      </div>

  </div>

@endsection
 --}}
@section('content')
      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">
      
          <!-- beginning of carousel -->
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="/image/1.jpg" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <h3>Best of all articles</h3>
        <p>We are on top always on top.AHOYAA</p>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="/image/2.jpg" alt="Second slide">
      <div class="carousel-caption d-none d-md-block">
      <p>Register To Post Your Articles <a href="{{ route('register') }}">Click Here</a></p>
    </div>
  </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="/image/3.jpg" alt="Third slide">
      <div class="carousel-caption d-none d-md-block">
      <h3>Thank you so much for visiting</h3>
    </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div> {{-- end of carousel --}}

<br>

        @foreach ($posts as $post)
          <!-- Blog Post -->
          <div class="card mb-4">
            <img src="{{$post->image}}" alt="Post Image">
            <div class="card-body">

              <h2 class="card-title">{{$post->title}}</h2>
              <p class="card-text">{{ substr($post->body, 0 ,300) }}{{ strlen($post->body) > 300 ? "...": "" }}</p>
              <a href="#" class="btn btn-primary">Read More &rarr;</a>
            </div>
            <div class="card-footer text-muted">
              Posted on {{ date('M j, Y ', strtotime($post->created_at))}} by
              <a href="#">{{ $post->user->name}}</a>
            </div>
          </div>

          @endforeach

          <!-- Pagination -->
          <ul class="pagination justify-content-center mb-4">
            <li class="page-item">
              <a class="page-link" href="{{  $posts->nextPageUrl() }}">&larr; Older</a>
            </li>
            <li class="page-item ">
              <a class="page-link" href="{{ $posts->previousPageUrl() }}">Newer &rarr;</a>
            </li>
          </ul>

        </div>

        <!-- Sidebar Widgets Column -->
        <div class="col-md-4">

          <!-- Search Widget -->
          <div class="card my-4">
            <h5 class="card-header">Search</h5>
            <div class="card-body">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                  <button class="btn btn-secondary" type="button">Go!</button>
                </span>
              </div>
            </div>
          </div>

          <!-- Categories Widget -->
          <div class="card my-4">
            <h5 class="card-header">Categories</h5>
            <div class="card-body">
              <div class="row">
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">Web Design</a>
                    </li>
                    <li>
                      <a href="#">HTML</a>
                    </li>
                    <li>
                      <a href="#">Freebies</a>
                    </li>
                  </ul>
                </div>
                <div class="col-lg-6">
                  <ul class="list-unstyled mb-0">
                    <li>
                      <a href="#">JavaScript</a>
                    </li>
                    <li>
                      <a href="#">CSS</a>
                    </li>
                    <li>
                      <a href="#">Tutorials</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <!-- Side Widget -->
          <div class="card my-4">
            <h5 class="card-header">Side Widget</h5>
            <div class="card-body">
              You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!
            </div>
          </div>

        </div>

      </div>
      <!-- /.row -->
@endsection
