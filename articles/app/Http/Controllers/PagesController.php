<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PagesController extends Controller
{
    //
    public function indexPage(){
    	$posts = Post::orderBy('created_at' ,'desc')->paginate(4);
    	return view('pages.welcome')->withPosts($posts);
    }
}
