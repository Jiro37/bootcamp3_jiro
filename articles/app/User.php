<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;



class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone_no',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public static function saveUsers($request){
      $newUser = new User;

      $newUser->name = $request['name'];
      $newUser->email = $request['email'];
      $newUser->password = bcrypt($request['password']);
      $newUser->phone_no = $request['phone_no'];

      $newUser->save();
      return redirect()->route('register');

    }
}
