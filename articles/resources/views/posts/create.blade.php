@extends('main')

@section('title', '| Create New Post')

@section('content')

	<div class="row">
		<div class="col-md-8 md-offset-2">
			<form action="{{ route('posts.store') }}" method="post">
				{{csrf_field()}}


			<h1>Create New Post</h1>
			<hr>
		
				<div class="form-group">
					<label>Title:</label>
					<input type="text" name="title" id="title" class="form-control">
				</div>
				<div class="form-group">
					<label>Body:</label>
					<textarea rows="5" class="form-control" id="body" name="body"></textarea>
				</div>
				<div class="form-group">
					<label>Image:</label>
					<input type="file" name="image">
				</div>
				<div>
					<input type="submit" name="submit" value="Create Post" class="btn btn-info">
				</div>
				<br>
			</form>
		</div>
	</div>


@endsection