<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'PagesController@indexPage')->name('home');

Route::get('/logout', 'UsersController@logout')->name('log-out');


Route::get('/users/register', 'UsersController@createUsers')->name('register');
Route::post('/users/register', 'UsersController@storeUsers')->name('save-users');


Route::get('/users/login', 'UsersController@loginUsers')->name('login');
Route::post('/users/login', 'UsersController@findUsers')->name('find-user');

Route::group(['middleware' => 'auth'], function(){
Route::resource('posts', 'PostController', ['except' => ['show,index']]);
});

// Route::resource('posts', 'PostController');