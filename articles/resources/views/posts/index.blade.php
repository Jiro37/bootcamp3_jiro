@extends('main')

@section('title', '| All Posts')

@section('stylesheets')
	<link href="{{ asset('css/jquery-confirm.min.css')}}" rel="stylesheet">
@endsection

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>All My Articles</h1>
		</div>
		<div class="col-md-2">
			<a href="{{ route('posts.create') }}" class="btn btn-lg btn-block btn-primary btn-h1-spacing">Create Post </a>
		</div>
			<hr>
	</div> <!--end of row -->

	<div class="row">
		<div class="col-md-12">
			<table class="table  table-striped table-hover">
				<thead class="thead-dark">
					<th>#</th>
					<th>Title</th>
					<th>Body</th>
					<th>Created At</th>
					<th>Action</th>
				</thead>
				<tbody>
					
					@foreach($posts as $post)

						<tr>
							<th>{{ $post->id }}</th>
							<td>{{ $post->title }}</td>
							<td>{{ substr($post->body, 0, 50) }} {{ strlen($post->body) > 50 ? "...": "" }} </td>
							<td>{{ date('M j, Y',strtotime($post->created_at)) }}</td>
							<td>
								<a href="{{ route('posts.show', $post->id) }}" class="btn btn-sm btn-success">View</a>
								<a href="{{ route('posts.edit', $post->id) }}" class="btn btn-info btn-sm">Edit</a>
								<a href="{{ route('posts.destroy', $post->id) }}" class="btn btn-sm btn-danger">Delete</a>
								{{-- <button class="btn btn-sm btn-danger delete-post"  onclick="{{ route('posts.destroy', $post->id) }}">Delete</button>
 --}}
						{{-- <form method="post" action="{{  route('posts.destroy' , array($post->id)) }}" id="DeleteSelectedPost">
						{{csrf_field()}} {{method_field('DELETE')}}
						<div class="col-sm-6">

						<input type="submit" class="btn btn-danger delete-post" name="delete" value="Delete">
						
					</div>
					</form> --}}
							</td>
						</tr>

					@endforeach

				</tbody>
			</table>

			<!-- Pagination -->

          <ul class="pagination justify-content-center mb-4">
            <li class="page-item">
              <a class="page-link" href=" {{  $posts->nextPageUrl() }}">&larr; Older</a>
            </li>
            
        	<li class="page-item"> 
        		<span class="page-link">{{ $posts->links()}}</span>
        	</li>
        	
            <li class="page-item">
              <a class="page-link" href=" {{ $posts->previousPageUrl() }}">Newer &rarr;</a>
            </li>
          </ul>
		</div>
	</div>

	@section('scripts')
		<script src=" {{ ('/js/jquery-confirm.min.js') }}"></script>

		<script type="text/javascript">
			
		$('.delete-post').click(function(e){
			e.preventDefault();
		 $.confirm({
        title: 'Delete Post!',
        content: 'Are you sure about this action, this may be serious',
        type: 'red',
        typeAnimated: true,
        buttons: {
            tryAgain: {
                text: 'Delete',
                btnClass: 'btn-red',
                action: function(){
                	return 'DeleteSelectedPost';
                }
            },
            close: function () {
            }
        }
    });			  
		});
			
			
		</script>
	@endsection


@stop