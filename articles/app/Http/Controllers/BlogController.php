<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function getIndex(){
    	$posts = Post::paginate(10);
    	return view('blog.index')->withPosts($posts);
    }


    public function getSingle($slug){
    	//fetch for db based on slug
    	$post = Post::where('slug', '=', $slug)->first();
    	//ret a view and pass the item into it
    	return view('blog.single')->withPost($post);
    }
}
