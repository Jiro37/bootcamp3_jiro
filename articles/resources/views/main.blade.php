<!DOCTYPE html>
<html lang="en">

 @include('partials._head')

 {{-- dynamically adding stylesheets --}}
      @yield('stylesheets') 

  <body>

@include('partials._nav')

  <div class="container">

    @include('partials._messages')
    
   @yield('content')

  </div>
    
@include('partials._footer')

@include('partials._javascripts')
   <!-- dynamically adding scripts --> 
    @yield('scripts')
  </body>
</html>