
@extends('main')

@section('content')

	<form action="{{ route('save-users')}}" method="post">
		{{csrf_field()}}



	<div class="card card-body">
		<h4 class="card-title">Sign In to Your Articles</h4>


  <div class="form-group">
    <label for="name">Name</label>
    <input type="name" class="form-control" id="name" name="name" placeholder="eg. Alex Hammer" required>
  </div>
  <div class="form-group">
    <label for="email">Email address</label>
    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required>
    <small id="email" class="form-text text-muted">We'll never share your email with anyone else.</small>
  </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
  </div>
  <div class="form-group">
    <label for="phone_no">Phone Number</label>
    <input type="phone_no" class="form-control" id="phone_no" name="phone_no" placeholder="eg. 0202211343" required>
  </div>
  <button type="submit" class="btn btn-primary">SIGN UP</button>
</div>
</form>


@endsection